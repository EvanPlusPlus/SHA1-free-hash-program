/*

Fonction de hashage SHA-1
C'est Evan qui l'a fait et pas un autre !
Mais je serais flatté que quelqu'un s'en serve dans un projet ( :

*/

// Inclusions des librairies

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef _WIN32
	#include <Windows.h>		
#endif


unsigned int functions(unsigned char t, unsigned int b, unsigned int c, unsigned int d){
	if( t >= 0 && t <= 19 )
		return (b & c) | ( ~b & d);
	if( t >= 20 && t <= 39 )
		return b ^ c ^ d;
	if( t >= 40 && t <= 59 )
		return (b & c) | (b & d) | (c & d);
	if( t >= 60 && t <= 79 )
		return b ^ c ^ d;
	return 0;
}

unsigned int constantWords(unsigned char t){
	if( t >= 0 && t <= 19 )
		return 0x5A827999;
	if( t >= 20 && t <= 39 )
		return 0x6ED9EBA1;
	if( t >= 40 && t <= 59 )
		return 0x8F1BBCDC;
	if( t >= 60 && t <= 79 )
		return 0xCA62C1D6;
	return 0;
}

unsigned long shiftRightByte(unsigned long value, unsigned char shift){
	unsigned long newValue = value;
	if(shift==0)
		return value;
	for(unsigned char i = 0; i < shift; i++){
		newValue = newValue >> 8;
	}
	return newValue;
}

unsigned int S(unsigned int n, unsigned int X){
	return (X << n) | (X >> 32-n);
}

struct BUFDATA {
  unsigned char *data;
  unsigned long length;
};

struct BUFDATA sha1String( unsigned char *str ){
	unsigned long strLength = strlen(str);
	unsigned long dataLength = 64;
	struct BUFDATA dataStructure;

	if( strLength + 9 > 64){
		dataLength = strLength + 9 + 64 - ((strLength + 9) % 64);
	}

	unsigned char *data = malloc(dataLength);
	if(data==NULL)
		exit(1);

	
	for( unsigned long i = strLength+1; i < dataLength-8; i++ ){
		data[i]=0;
	}

	for(unsigned char i = 0; i < 8; i++){
		data[dataLength-1-i] = (shiftRightByte(strLength*8, i) & 0xFF);
	}
	//data[63]=24;

	strcpy(data, str);

	data[strLength] = 0x80;

	dataStructure.data = data;
	dataStructure.length = dataLength;
	return dataStructure;
}

void SHA1(unsigned char *input, unsigned char *output){
	struct BUFDATA dataStructure = sha1String( input );
	unsigned char *data = dataStructure.data;

	unsigned int H[5] = {0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0};
	unsigned int words[80];

	for(unsigned long blkI = 0; blkI < dataStructure.length/64; blkI++){
		for(unsigned char i = 0; i < 80; i++){
			words[i]=0;
		}
		for(unsigned int i = 0; i < 64; i += 4)
		{
			words[i/4] = (data[i + blkI*64] << 3*8) | (data[i+1 + blkI*64] << 2*8) | (data[i+2 + blkI*64] << 1*8) | data[i+3 + blkI*64];
		}

		for(unsigned int t = 16; t < 80; t++){
			words[t]=S(1, words[t-3] ^ words[t-8] ^ words[t-14] ^ words[t-16]);
		}


		unsigned int A, B, C, D, E;
		A = H[0];
		B = H[1];
		C = H[2];
		D = H[3];
		E = H[4];

		for(unsigned int t = 0; t < 80; t++){
			unsigned int temp = S(5, A) + functions(t, B, C, D) + E + words[t] + constantWords(t);
			E = D;
			D = C;
			C = S(30, B);
			B = A;
			A = temp;
		}

		H[0] = H[0] + A;
		H[1] = H[1] + B;
		H[2] = H[2] + C;
		H[3] = H[3] + D;
		H[4] = H[4] + E;
	}

	free(data);

	for(unsigned char i = 0; i < 5; i++){
		for(unsigned char i2 = 0; i2 < 4; i2++){
			output[i*4+i2] = (shiftRightByte(H[i], 3-i2) & 0xFF);
		}
	}
}

int main(int argc, char const *argv[])
{
	
	// Présence d'un argument à l'appel du programme ? Sinon, arrêt du programme.

	if(argc != 2){
		#ifdef _WIN32
			HANDLE prompt = GetStdHandle(STD_OUTPUT_HANDLE);
			CONSOLE_SCREEN_BUFFER_INFO bufInfo;
			GetConsoleScreenBufferInfo(prompt, &bufInfo);		
			SetConsoleTextAttribute(prompt, FOREGROUND_RED);
			printf("[ERREUR] Arguments manquants (%d au lieu de 1)\n", argc-1);
			SetConsoleTextAttribute(prompt, bufInfo.wAttributes);
			return 160; // ERROR BAD ARGUMENT
		#else
			printf("[ERREUR] Arguments manquants (%d au lieu de 1)\n", argc-1);
			return 2;
		#endif
	}

	unsigned char hash[20];
	SHA1((unsigned char *)argv[1], hash);
	printf("SHA1 = ");
	for(unsigned char i = 0; i < 20; i++){
		printf("%02x", hash[i]);
	}
	printf("\n");

	return 0;
}